import Form from "./components/Form";
// import Result from "./components/Result";
import {
  // Button,
  // Input,
  // FormLabel,
  // Stack,
  // TextField,
  Container,
} from "@mui/material";
import TopBar from "./components/TopBar";

function App() {
  return (
    <Container
      maxWidth="sm"
      sx={{
        margin: "2rem auto",
      }}
    >
      <TopBar />
      <Form />
    </Container>
  );
}

export default App;
