import { screen, render } from "@testing-library/react";
import Result from "./Result";

const matches = [
  {
    BlockType: "LINE",
    Confidence: 99.2922134399414,
    Geometry: {
      BoundingBox: {
        Height: 0.03371236473321915,
        Left: 0.10282602161169052,
        Top: 0.2883140742778778,
        Width: 0.28279370069503784,
      },
      Polygon: [
        {
          X: 0.1035851240158081,
          Y: 0.2883140742778778,
        },
        {
          X: 0.38561972975730896,
          Y: 0.2923755943775177,
        },
        {
          X: 0.384860634803772,
          Y: 0.32202643156051636,
        },
        {
          X: 0.10282602161169052,
          Y: 0.31796494126319885,
        },
      ],
    },
    Id: "8274c922-4c94-4e42-8b71-66629ac12a74",
    Relationships: [
      {
        Ids: [
          "3861c417-78da-4f51-8953-b49f980777e3",
          "106e092e-fce5-4dce-889e-1bc778d6b031",
          "9669f184-ac29-4359-9a19-a3db507f916a",
        ],
        Type: "CHILD",
      },
    ],
    Text: "DETOL CARE 175G",
  },
  {
    BlockType: "LINE",
    Confidence: 98.27626037597656,
    Geometry: {
      BoundingBox: {
        Height: 0.03330123797059059,
        Left: 0.10074026137590408,
        Top: 0.31377559900283813,
        Width: 0.28321361541748047,
      },
      Polygon: [
        {
          X: 0.10148867964744568,
          Y: 0.31377559900283813,
        },
        {
          X: 0.38395386934280396,
          Y: 0.3178432881832123,
        },
        {
          X: 0.38320544362068176,
          Y: 0.3470768332481384,
        },
        {
          X: 0.10074026137590408,
          Y: 0.3430091142654419,
        },
      ],
    },
    Id: "d7c1035f-77a4-47b7-b0f4-972dbc66c468",
    Relationships: [
      {
        Ids: [
          "8f7043b1-4c84-406d-9fe3-3794ba8978e7",
          "53005fbd-a2b6-422a-a9a6-ef120062630f",
          "17e38bcc-998f-4c59-bd58-205064787188",
        ],
        Type: "CHILD",
      },
    ],
    Text: "DETOL EVEN 175G",
  },
  {
    BlockType: "LINE",
    Confidence: 99.14380645751953,
    Geometry: {
      BoundingBox: {
        Height: 0.03356844186782837,
        Left: 0.0985598936676979,
        Top: 0.34032702445983887,
        Width: 0.28448376059532166,
      },
      Polygon: [
        {
          X: 0.09931468218564987,
          Y: 0.34032702445983887,
        },
        {
          X: 0.38304364681243896,
          Y: 0.3444129228591919,
        },
        {
          X: 0.3822888433933258,
          Y: 0.37389546632766724,
        },
        {
          X: 0.0985598936676979,
          Y: 0.3698095381259918,
        },
      ],
    },
    Id: "6b326e1b-78e2-469b-8347-11a198f82585",
    Relationships: [
      {
        Ids: [
          "6cf36d67-125d-4afc-baf9-3198933c86bf",
          "4ad39493-481d-46a2-8405-febee8262149",
          "2972e37c-3333-4ad2-9f81-0508b50a06f3",
        ],
        Type: "CHILD",
      },
    ],
    Text: "DETOL NRSH 175G",
  },
  {
    BlockType: "LINE",
    Confidence: 98.81292724609375,
    Geometry: {
      BoundingBox: {
        Height: 0.03219766914844513,
        Left: 0.09712474793195724,
        Top: 0.3678656816482544,
        Width: 0.28538963198661804,
      },
      Polygon: [
        {
          X: 0.09784410148859024,
          Y: 0.3678656816482544,
        },
        {
          X: 0.3825143873691559,
          Y: 0.3719651699066162,
        },
        {
          X: 0.3817950487136841,
          Y: 0.4000633656978607,
        },
        {
          X: 0.09712474793195724,
          Y: 0.3959639072418213,
        },
      ],
    },
    Id: "52ab8ac4-30ee-4593-8dec-eaa0c88f73f4",
    Relationships: [
      {
        Ids: [
          "ee177bef-1063-4adf-ae6f-a767710bf9bc",
          "5fc90da1-650d-4d20-b4e9-52621692a4f7",
          "ea03a918-0afa-4823-b3ce-3f019e3cf0de",
        ],
        Type: "CHILD",
      },
    ],
    Text: "DETOL POMG 175G",
  },
  {
    BlockType: "LINE",
    Confidence: 98.69718933105469,
    Geometry: {
      BoundingBox: {
        Height: 0.0302518792450428,
        Left: 0.09551691263914108,
        Top: 0.39586588740348816,
        Width: 0.30491939187049866,
      },
      Polygon: [
        {
          X: 0.09617922455072403,
          Y: 0.39586588740348816,
        },
        {
          X: 0.40043631196022034,
          Y: 0.4002474248409271,
        },
        {
          X: 0.3997739851474762,
          Y: 0.42611774802207947,
        },
        {
          X: 0.09551691263914108,
          Y: 0.4217362403869629,
        },
      ],
    },
    Id: "f65e1a4b-94a5-4594-9bc0-43c28490738e",
    Relationships: [
      {
        Ids: [
          "cc7024c0-f547-4d07-be5f-42b540b93c3f",
          "aaae16ba-b78b-4a91-8301-d59979058337",
          "8b9130c3-6d98-42a3-8737-054b88886a4c",
        ],
        Type: "CHILD",
      },
    ],
    Text: "DETOL RFRSH 175G",
  },
  {
    BlockType: "LINE",
    Confidence: 99.1259994506836,
    Geometry: {
      BoundingBox: {
        Height: 0.029868178069591522,
        Left: 0.0934065505862236,
        Top: 0.42293834686279297,
        Width: 0.2873486876487732,
      },
      Polygon: [
        {
          X: 0.09406551718711853,
          Y: 0.42293834686279297,
        },
        {
          X: 0.3807552456855774,
          Y: 0.4270668923854828,
        },
        {
          X: 0.38009628653526306,
          Y: 0.4528065323829651,
        },
        {
          X: 0.0934065505862236,
          Y: 0.4486779570579529,
        },
      ],
    },
    Id: "cb1903c8-cdb5-44ba-bd59-9f1aeb2fe5db",
    Relationships: [
      {
        Ids: [
          "56ee8307-c3c1-4754-acc1-755b6fe9942b",
          "585a4a68-e858-42ba-8fd9-6607b16c2161",
          "2f54faa1-7d6a-43a8-8bcd-98ae80f2d04d",
        ],
        Type: "CHILD",
      },
    ],
    Text: "DETOL SKIN 175G",
  },
];

test("Checks if List renders", async () => {
  render(<Result matches={matches} />);
  const itemsList = screen.getByTestId("match-items");
  expect(itemsList).toBeVisible();
});
