import { render, screen } from "@testing-library/react";
import Form from "./Form";

test("render query input", () => {
  render(<Form />);
  const queryInput = screen.getByTestId("query-input");
  expect(queryInput).toBeVisible();
});

test("render percentage input", () => {
  render(<Form />);
  const percentageInput = screen.getByTestId("percentage-input");
  expect(percentageInput).toBeVisible();
});

test("render image input", () => {
  render(<Form />);
  const imageInput = screen.getByTestId("image-input");
  expect(imageInput).toBeVisible();
});

test("render button input", () => {
  render(<Form />);
  const buttonInput = screen.getByTestId("button-input");
  expect(buttonInput).toBeVisible();
});
