import { Box, List, ListItem, Stack, Typography } from "@mui/material";

const Result = ({ matches }) => {
  let lines = matches.filter((match) => match.BlockType === "LINE");
  return (
    <Stack sx={{ marginTop: "3rem" }}>
      {lines.length ? (
        <>
          <Typography variant="h4" align="center">
            Matches:
          </Typography>
          <List data-testid="match-items">
            {lines.map((item) => (
              <ListItem key={item.Id}>
                <Box
                  sx={{
                    width: "100%",
                    alignItems: "center",
                    display: "flex",
                    justifyContent: "space-between",
                    borderBottom: "1px solid gray",
                    padding: "1rem",
                  }}
                >
                  <Typography variant="body1">{item.Text}</Typography>
                  <Typography>{item.Confidence.toFixed(2)}%</Typography>
                </Box>
              </ListItem>
            ))}
          </List>
        </>
      ) : (
        <></>
      )}
    </Stack>
  );
};

export default Result;
