import axios from "axios";
import { useState } from "react";
import {
  Button,
  Input,
  FormLabel,
  TextField,
  Snackbar,
  Stack,
} from "@mui/material";
import { LoadingButton } from "@mui/lab";
import Result from "./Result";

const Form = () => {
  // Response
  const [matches, setMatches] = useState([]);

  // Loading Button
  const [loading, setLoading] = useState(false);

  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };
  const handleSubmit = () => {
    // convert image to base64
    const image = document.getElementById("image").files[0];
    const query = document.getElementById("query").value;
    const percentage = document.getElementById("percentage").value;
    if (image === undefined || query === "" || percentage === "") {
      handleOpen();
      return;
    }
    const reader = new FileReader();
    reader.readAsDataURL(image);
    reader.onloadend = async () => {
      const base64 = reader.result;
      setLoading(true);
      await axios
        .post("http://localhost:3100", {
          image: { base64 },
          query: query,
          confidenceMinimum: parseFloat(percentage),
        })
        .then((res) => {
          if (res.status === 200) {
            if (res.data.matches.length === 0) alert("No matches");
            setMatches(res.data.matches);
          }
        })
        .catch((err) => {
          alert("Operation failed");
          return err.message;
        });
      setLoading(false);
    };
  };
  return (
    <Stack>
      <Stack spacing={4}>
        <TextField
          id="query"
          required
          label="Query"
          data-testid="query-input"
        />
        <TextField
          type="number"
          id="percentage"
          required
          label="Confidence Percentage"
          placeholder="90"
          data-testid="percentage-input"
        />
        <FormLabel htmlFor="image">
          Select Image: <br />
          <Input
            data-testid="image-input"
            required
            type="file"
            name="image"
            id="image"
          />
        </FormLabel>
        {loading ? (
          <LoadingButton loading variant="contained">
            Loading
          </LoadingButton>
        ) : (
          <Button
            data-testid="button-input"
            width="50%"
            variant="contained"
            onClick={handleSubmit}
            type="button"
          >
            Upload
          </Button>
        )}
        <Snackbar
          open={open}
          autoHideDuration={2000}
          onClose={handleClose}
          message="Please complete all fields"
        />
      </Stack>
      <Result matches={matches} />
    </Stack>
  );
};

export default Form;
