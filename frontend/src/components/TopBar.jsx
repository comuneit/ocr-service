import { AppBar, Box, Typography } from "@mui/material";

const TopBar = () => {
  return (
    <Box sx={{ margin: "8rem" }}>
      <AppBar sx={{ padding: "1rem" }}>
        <Typography position="static" variant="h5" align="center">
          Receipt Checker
        </Typography>
      </AppBar>
    </Box>
  );
};

export default TopBar;
