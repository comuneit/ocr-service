# Get up and running
0. Fork this repo.
1. Create an `development.env` by copying `env.template` and updating the values as needed.
2. In the `backend` folder run `npm install`.
3. Install the AWS CLI, instructions here: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
4. run `aws configure` and provide the credentials as requested.
4.1 For region, supply `eu-west-1`.
5. Run `node index.js` in the `backend` folder to have the backend running.

# Postman collection
Use the postman collection to see how to use the backend.

# Build your frontend
In the `frontend` folder create a react app to demonstrate the use of the api.
The frontend should:
* Present a suitable UI that allows the user to provide an 
  - image and
  - a query string.
  - the required confidence percentage.
* Allow the user to post their input data to the api
  - indicate loading state
  - present the results in a meaningful way.
* Include suitable tests, i.e Jest/Mocha or whichever testing library you prefer.

You are free to use quality of life libraries such material-ui, axios etc.

Please submit your code as a pull request to `main`, with a branch name `feature-ocr-frontend-[FIRSTNAME-SURNAME].`
Where `[FIRSTNAME-SURNAME]` is your names.