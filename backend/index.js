const fs = require("fs");
const serverless = require("serverless-http");
const bodyParser = require("koa-bodyparser");
const Koa = require("koa");
const cors = require("@koa/cors");
const { Base64Encode } = require("base64-stream");
const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args));
require("dotenv").config({
  path: "./development.env",
});

const DEBUG = process.env.NODE_ENV === "development";

function log() {
  if (DEBUG) {
    console.log(arguments);
  }
}

log('DEBUG MODE ENABDLED');

var app = new Koa();
app.use(bodyParser());
var {
  TextractClient,
  AnalyzeDocumentCommand,
} = require("@aws-sdk/client-textract");
var options = {
  origin: "*",
  allowMethods: ["POST", "OPTIONS"],
  allowHeaders: "*",
};
app.use(cors(options));
var REGION = "eu-west-1";
var textractClient = new TextractClient({
  region: REGION,
  aws_access_key_id: process.env.AWS_ACCESS_KEY_ID,
  aws_secret_access_key: process.env.AWS_SECRET_ACCESS_KEY,
});
var params = {
  FeatureTypes: ["TABLES"],
};

const getImageDataFromUrl = (url) => {
  return fetch(url).then((res) => {
    return new Promise((resolve, reject) => {
      let chunks = [];
      let myStream = res.body.pipe(new Base64Encode());
      myStream.on("data", (chunk) => {
        chunks = chunks.concat(chunk);
      });
      myStream.on("end", () => {
        resolve(chunks.toString("base64"));
      });
    });
  });
};

var findMatches = async (response, query, ConfidenceMinimum) => {
  try {
    const results = Object.values(response.Blocks).reduce((matches, block) => {
      if ("Text" in block && block.Text !== void 0) {
        if (
          block.Confidence > ConfidenceMinimum &&
          `${block.Text}`.toLowerCase().includes(query.toLowerCase())
        ) {
          console.log(`Text: ${block.Text} includes ${query}`);
          matches.push(block);
        }
      }
      return matches;
    }, []);
    return results;
  } catch (err) {
    console.log("Error", err);
  }
};
var analyze_document_text = async (analyzeParams, query, ConfidenceMinimum) => {
  try {
    const analyzeDoc = new AnalyzeDocumentCommand(analyzeParams);
    const response = await textractClient.send(analyzeDoc);
    const matches = await findMatches(response, query, ConfidenceMinimum);
    return { matches, queryFound: matches.length > 0 };
  } catch (err) {
    console.log("Error", err);
  }
};
app.on("error", (err) => {
  console.error("server error", err);
});

app.use(async (ctx) => {
  const { event } =
    process.env.NODE_ENV === "development" ? {} : ctx.req.apiGateway;
  const body =
    process.env.NODE_ENV === "development"
      ? ctx.request.body
      : JSON.parse(event.body);
  
  log("event", Object.keys(body), typeof body);
  console.log('Payload', { ...body, image: { ...body.image, base64: 'REDACTED' } });
  let base64Only = "";
  if (body.image) {
    if (body.image.url) {
      log('Processing image url...');
      base64Only = await getImageDataFromUrl(body.image.url);
      log('Image data: ', base64Only);
    } else {
      base64Only = body.image.base64.replace(
        /data:image\/(jpeg|png)\;base64\,/,
        ""
      );
    }
    const ocrParams = {
      ...params,
      Document: {
        Bytes: Buffer.from(base64Only, "base64"),
      },
    };
    log('ocrParams', ocrParams);
    const ocrResponse = await analyze_document_text(
      ocrParams,
      body.query,
      body.confidenceMinimum
    );
    log('ocrResponse', ocrResponse);
    ctx.body = ocrResponse;
  } else {
    ctx.status = 400;
    ctx.body = {
      message:
        "Please ensure your request includes a confidenceMinimum, query and image: { base64 } keys.",
    };
  }
  ctx.set("Content-Type", `application/json`);
  ctx.set("Access-Control-Allow-Origin", `*`);
});

console.log(process.env.NODE_ENV, "log");

if (process.env.NODE_ENV === "development") {
  app.listen(3100);
  console.log("Backend listening at http://localhost:3100\n");
} else {
  module.exports.handler = serverless(app);
}
